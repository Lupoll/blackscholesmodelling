from math import sqrt, log, exp, erf
import random
from numpy import arange
import matplotlib.pyplot as plt
import pandas as pd
import os

S0 = 100.0  # S0 = Stock price
strikes = [i for i in range(50, 150)]  # Exercise prices range
T = 1  # T = Time to expiration
r = 0.01  # r = risk-free interest rate
q = 0.02  # q = dividend yield
vol = 0.2  # vol = volatility
Nsteps = 100  # Number or steps in MC


# 1. Black-Scholes formula:

d1 = (log(S0/strikes) + (r-q+sig^2/2)*T)/sig*sqrt(T)

d2 = (log(S0/strikes) + (r-q-sig^2/2)*T)/sig*sqrt(T)

C0 = S0*XXXX(d1) - strikes*exp(-r*T)*XXX(d2)